FROM rust:1.48 as builder
CMD mkdir -p /usr/src/yagc
WORKDIR /usr/src/yagc
COPY . .
RUN cargo install --path .

FROM debian:buster-slim
RUN apt-get update && apt-get install -y libssl-dev && rm -rf /var/lib/apt/lists/*
COPY --from=builder /usr/local/cargo/bin/yagc /usr/local/bin/yagc
CMD ["yagc"]
