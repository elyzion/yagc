# About

YaGC is a simply CronJob based ConfigMap cleaner for Kubernetes, written in Rust.

## GC selection


ConfigMaps with the `yagc/gc=true` label are eligible for garbage collection. Eligible ConfigMaps will be deleted if they are not referenced by any pods. 

## Execution flow

`YaGC` selects all pods in the target namespace and all configmaps with the `yagc/gc=true` label.

It then builds a list of mounted configmaps that is uses to filter the list of cleanable configmaps. This produces a list of orphaned configmaps. This list is then looped over, deleting configmaps one by one.

*Note*: The current deletion logic is suited to my requirements. If a significant amount of configmaps need to be deleted on every execution, make sure that it will not be placing stress on your kubernetes API endpoint.

# Installation 

## Kubernetes

Please refer to `deployment.yaml` for an example deployment configuration.

## Local

Please checkout the repository and then install the command with `cargo install`, or run the command with `cargo run`

# Usage

The `yagc` executable takes the following arguments: 

```
YAGC 0.1.0
Berthold Alheit <behoalheit@gmail.com>
Garbage cleans unreferenced config maps from Kubernetes.

USAGE:
    yagc [FLAGS] --namespace <NAMESPACE>

FLAGS:
    -d, --dryrun     Perform a dryrun that doesn't delete anything
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -n, --namespace <NAMESPACE>    Sets the target kubernetes namespace [env: NAMESPACE=]

```

# Development

Kubernetes resources for local monkey testing are located in `kubernetes/`. Apply the resources to your local development cluster and then execute `yagc` to test it manually.

## Actual automated testing?

Yes, this would be nice to have at sometime. This is however pending on some more work regarding running kubernetes cluster for integration test purposes in Travis or CircleCI.
