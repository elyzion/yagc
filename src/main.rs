use anyhow::Result;
use clap::{crate_authors, crate_version, App, Arg};
use k8s_openapi::api::core::v1::{ConfigMap, Container, Pod};
use kube::{
    api::{Api, DeleteParams, ListParams},
    Client,
};
use tracing::{debug, info};
use tracing_subscriber;

use std::env;

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::fmt::init();
    let client = Client::try_default().await?;

    let matches = App::new("YAGC")
        .version(crate_version!())
        .author(crate_authors!())
        .about("Garbage cleans unreferenced config maps from Kubernetes.")
        .arg(
            Arg::new("NAMESPACE")
                .short('n')
                .long("namespace")
                .env("NAMESPACE")
                .value_name("NAMESPACE")
                .about("Sets the target kubernetes namespace")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::new("DRYRUN")
                .short('d')
                .long("dryrun")
                .env("DRYRUN")
                .default_value("true")
                .about("Perform a dryrun that doesn't delete anything"),
        )
        .get_matches();

    let namespace = matches.value_of("NAMESPACE").unwrap();
    let dryrun = matches.is_present("DRYRUN");

    // Basic execution flow:
    // 1. Read the list of pod mounted config maps
    // 2. Read the list of configmaps annotated with "yagc/gc=true".
    // 3. Build a list of orphaned config maps.
    // 5. Trim orphaned configmaps labeled with yagc/gc=true.

    let pod_api: Api<Pod> = Api::namespaced(client.clone(), &namespace);
    info!("Loading pods for namespace {:?}", namespace);
    let lp = ListParams::default();
    let pods = pod_api.list(&lp).await?;
    // TODO: ENV vars should be supported
    let mut mounted_config_maps: Vec<String> = pods
        .iter()
        .filter_map(|pod| pod.spec.as_ref())
        .filter_map(|spec| spec.volumes.as_ref())
        .flatten()
        .filter_map(|v| v.config_map.as_ref())
        .filter_map(|cm| cm.name.clone())
        .collect();
    debug!("Found mounted config_maps: {:#?}", mounted_config_maps);

    let containers: Vec<Container> = pods
        .iter()
        .filter_map(|pod| pod.spec.as_ref())
        .filter_map(|spec| {
            let mut containers: Vec<Container> = spec.containers.clone();
            let init_containers: Vec<Container> =
                spec.init_containers.as_ref().unwrap_or(&Vec::new()).clone();
            containers.extend(init_containers);
            Some(containers)
        })
        .flatten()
        .collect();

    let referenced_env_from_config_maps: Vec<String> = containers
        .iter()
        .filter_map(|container| container.env_from.as_ref())
        .flatten()
        .filter_map(|env_from| env_from.config_map_ref.as_ref())
        .filter_map(|config_map_ref| config_map_ref.name.clone())
        .collect();
    debug!(
        "Found referenced config_maps: {:#?}",
        referenced_env_from_config_maps
    );
    mounted_config_maps.extend(referenced_env_from_config_maps);

    let referenced_env_config_maps: Vec<String> = containers
        .iter()
        .filter_map(|container| container.env.as_ref())
        .flatten()
        .filter_map(|env| env.value_from.as_ref())
        .filter_map(|value_from| value_from.config_map_key_ref.as_ref())
        .filter_map(|config_map_key_ref| config_map_key_ref.name.clone())
        .collect();
    debug!(
        "Found referenced config_maps: {:#?}",
        referenced_env_config_maps
    );

    mounted_config_maps.extend(referenced_env_config_maps);

    let config_map_api: Api<ConfigMap> = Api::namespaced(client, &namespace);
    info!("Loading config maps for namespace {:?}", namespace);
    let lp = ListParams::default().labels("yagc/gc=true");
    let config_maps = config_map_api.list(&lp).await?;
    let orphans: Vec<ConfigMap> = config_maps
        .into_iter()
        .filter(|cm| {
            // is this configmap mounted as a volume?
            mounted_config_maps
                .iter()
                .find(|v| match cm.metadata.name.as_ref() {
                    Some(name) => v.as_str() == name.as_str(),
                    None => false,
                })
                .is_none()
        })
        .collect();

    for orphan in orphans {
        info!(
            "{}Deleting orphaned config map: {}",
            if dryrun { "[DRYRUN] " } else { "" },
            orphan.metadata.name.clone().unwrap()
        );
        if dryrun == false {
            config_map_api
                .delete(
                    &orphan.metadata.name.clone().unwrap(),
                    &DeleteParams::default(),
                )
                .await?;
        }
    }

    Ok(())
}
